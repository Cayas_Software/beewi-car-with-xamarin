// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace BeeWiCar.IOS
{
	[Register ("MainViewController")]
	partial class MainViewController
	{
		[Outlet]
		UIKit.UIButton btnBackwards { get; set; }

		[Outlet]
		UIKit.UIButton btnForward { get; set; }

		[Outlet]
		UIKit.UIButton btnLeft { get; set; }

		[Outlet]
		UIKit.UIButton btnRight { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnForward != null) {
				btnForward.Dispose ();
				btnForward = null;
			}

			if (btnLeft != null) {
				btnLeft.Dispose ();
				btnLeft = null;
			}

			if (btnRight != null) {
				btnRight.Dispose ();
				btnRight = null;
			}

			if (btnBackwards != null) {
				btnBackwards.Dispose ();
				btnBackwards = null;
			}
		}
	}
}
