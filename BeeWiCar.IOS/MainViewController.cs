﻿using System;
using Foundation;
using UIKit;
using ExternalAccessory;

namespace BeeWiCar.IOS
{
    public partial class MainViewController : UIViewController
    {
        EASession session;

        public MainViewController()
            : base("MainViewController", null)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            btnForward.TouchUpInside += delegate
            {
                SendCommand(BeeWiCarCommands.Forward_Stop);
            };

            btnForward.TouchDown += delegate
            {
                SendCommand(BeeWiCarCommands.Forward_Go);
            };

            btnLeft.TouchUpInside += delegate
            {
                SendCommand(BeeWiCarCommands.Left_Stop);
            };

            btnLeft.TouchDown += delegate
            {
                SendCommand(BeeWiCarCommands.Left_Go);
            };

            btnRight.TouchUpInside += delegate
            {
                SendCommand(BeeWiCarCommands.Right_Stop);
            };

            btnRight.TouchDown += delegate
            {
                SendCommand(BeeWiCarCommands.Right_Go);
            };

            btnBackwards.TouchUpInside += delegate
            {
                SendCommand(BeeWiCarCommands.Backwards_Stop);
            };

            btnBackwards.TouchDown += delegate
            {
                SendCommand(BeeWiCarCommands.Backwards_Go);
            };

            OpenSession();
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);

            CloseSession();
        }

        void SendCommand(BeeWiCarCommands command)
        {
            try
            {
                if (session == null)
                {
                    new UIAlertView("BeeWi Car", "No connection established", null, "OK").Show();
                    return;
                }

                if (!session.Accessory.Connected)
                {
                    new UIAlertView("BeeWi Car", "Car not connected", null, "OK").Show();
                    return;
                }

                session.OutputStream.Write(new byte[]{ (byte)command }, 0, 1);
            }
            catch (Exception ex)
            {
                new UIAlertView("BeeWi Car", ex.Message, null, "OK").Show();
            }
        }

        void OpenSession()
        {
            var connectedAccessories = EAAccessoryManager.SharedAccessoryManager.ConnectedAccessories;
            EAAccessory beeWiCar = null;

            foreach (var accessory in connectedAccessories)
            {
                foreach (var protocolString in accessory.ProtocolStrings)
                {
                    protocolString.Contains("com.beewi.controlleur");
                    beeWiCar = accessory;
                    break;
                }
            }

            if (beeWiCar != null)
            {
                try
                {
                    session = new EASession(beeWiCar, "com.beewi.controlleur");
                    session.Accessory.Disconnected += delegate
                    {
                        CloseSession();
                        new UIAlertView("BeeWi Car", "BeeWi car disconnected", null, "OK").Show();
                    };

                    session.InputStream.Schedule(NSRunLoop.Current, NSRunLoop.NSDefaultRunLoopMode);
                    session.InputStream.Open();

                    session.OutputStream.Schedule(NSRunLoop.Current, NSRunLoop.NSDefaultRunLoopMode);
                    session.OutputStream.Open();
                }
                catch (Exception ex)
                {
                    new UIAlertView("BeeWi Car", "Ups something went wrong.", null, "OK").Show();
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                new UIAlertView("BeeWi Car", "No BeeWi car connected", null, "OK").Show();
            }
        }

        void CloseSession()
        {
            if (session == null)
                return;

            session.InputStream.Close();
            session.InputStream.Dispose();

            session.OutputStream.Close();
            session.OutputStream.Dispose();
            session.Dispose();
            session = null;
        }
    }

    public enum BeeWiCarCommands
    {
        Forward_Stop = 0,
        Forward_Go = 1,
        Backwards_Stop = 2,
        Backwards_Go = 3,
        Left_Stop = 4,
        Left_Go = 5,
        Right_Stop = 6,
        Right_Go = 7
    }
}