﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using System.Linq;
using Java.Util;

namespace BeeWiCar.Droid
{
    [Activity(Label = "BeeWiCar.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Button front, right, left, back;
        Android.Bluetooth.BluetoothDevice currentDevice;
        Android.Bluetooth.BluetoothSocket socket;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            front = FindViewById<Button>(Resource.Id.btnForward);
            right = FindViewById<Button>(Resource.Id.btnRight);
            left = FindViewById<Button>(Resource.Id.btnLeft);
            back = FindViewById<Button>(Resource.Id.btnBackwards);

            var bluetoothDefaultAdapter = Android.Bluetooth.BluetoothAdapter.DefaultAdapter;

            if (bluetoothDefaultAdapter == null)
            {
                Toast.MakeText(this, "Your device does not support Bluetooth", ToastLength.Long).Show();
                return;
            }

            if (!bluetoothDefaultAdapter.IsEnabled)
                StartActivityForResult(new Intent(Android.Bluetooth.BluetoothAdapter.ActionRequestEnable), 1);

            var deviceList = new List<string>();
            foreach (var device in bluetoothDefaultAdapter.BondedDevices)
            {
                deviceList.Add(String.Format("{0} ({1})", device.Name, device.Address));
            }

            var deviceAdapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItemMultipleChoice, deviceList.ToArray());
            var listView = FindViewById<ListView>(Resource.Id.listView1);
            listView.ChoiceMode = ChoiceMode.Single;
            listView.Adapter = deviceAdapter;
            listView.ItemClick += async delegate(object sender, AdapterView.ItemClickEventArgs e)
            {
                if (currentDevice != null && currentDevice.Address != bluetoothDefaultAdapter.BondedDevices.ToList()[e.Position].Address)
                    socket.Close();

                currentDevice = bluetoothDefaultAdapter.BondedDevices.ToList()[e.Position];
                socket = currentDevice.CreateRfcommSocketToServiceRecord(UUID.FromString("00001101-0000-1000-8000-00805f9b34fb"));

                try
                {
                    if (!socket.IsConnected)
                        await socket.ConnectAsync();
                }
                catch (Exception ex)
                {
                    Toast.MakeText(this, "Could not connect to device", ToastLength.Short).Show();

                    currentDevice = null;
                    socket = null;
                }
            };

            front.Touch += delegate(object sender, View.TouchEventArgs e)
            {
                if (e.Event.Action == MotionEventActions.Down || e.Event.Action == MotionEventActions.Move)
                    SendCommand(BeeWiCarCommands.Forward_Go);
                else
                    SendCommand(BeeWiCarCommands.Forward_Stop);
            };

            back.Touch += delegate(object sender, View.TouchEventArgs e)
            {
                if (e.Event.Action == MotionEventActions.Down || e.Event.Action == MotionEventActions.Move)
                    SendCommand(BeeWiCarCommands.Backwards_Go);
                else
                    SendCommand(BeeWiCarCommands.Backwards_Stop);
            };

            left.Touch += delegate(object sender, View.TouchEventArgs e)
            {
                if (e.Event.Action == MotionEventActions.Down || e.Event.Action == MotionEventActions.Move)
                    SendCommand(BeeWiCarCommands.Left_Go);
                else
                    SendCommand(BeeWiCarCommands.Left_Stop);
            };

            right.Touch += delegate(object sender, View.TouchEventArgs e)
            {
                if (e.Event.Action == MotionEventActions.Down || e.Event.Action == MotionEventActions.Move)
                    SendCommand(BeeWiCarCommands.Right_Go);
                else
                    SendCommand(BeeWiCarCommands.Right_Stop);
            };
        }

        void SendCommand(BeeWiCarCommands command)
        {
            try
            {
                if (socket == null)
                {
                    Toast.MakeText(this, "No device selected", ToastLength.Short).Show();
                    return;
                }

                if (!socket.IsConnected)
                {
                    Toast.MakeText(this, "Device not connected", ToastLength.Short).Show();
                    return;
                }

                if (socket.OutputStream.CanWrite)
                {
                    socket.OutputStream.Write(new byte[]{ (byte)command }, 0, 1);
                    socket.OutputStream.Flush();
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
            }
        }
    }

    public enum BeeWiCarCommands
    {
        Forward_Stop = 0,
        Forward_Go = 1,
        Backwards_Stop = 2,
        Backwards_Go = 3,
        Left_Stop = 4,
        Left_Go = 5,
        Right_Stop = 6,
        Right_Go = 7
    }
}